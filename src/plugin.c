/* gmpc-lastfmradio (GMPC plugin)
 * Copyright (C) 2008-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <string.h>
#include <gmpc/plugin.h>
#include <gmpc/config1.h>
#include <gmpc/gmpc-metaimage.h>
#include <config.h>
#include <gmpc/playlist3-messages.h>
#include "lfr_plugin.h"
#include "last-fm.h"


/* The plugin structure */
gmpcPlugin plugin;

/* This keeps a reference to the browser in the left row */
static GtkTreeRowReference *lfr_ref = NULL;
/* The widget that holds the browser view */
static GtkWidget *lfr_vbox = NULL;

/* Last.FM object */
static LastFM *lfm = NULL;
/**
 * Destroy, init and saving
 */
static void lfr_init(void)
{

    gchar *path = gmpc_plugin_get_data_path(&plugin);
    gchar *url = g_build_path(G_DIR_SEPARATOR_S,path, "lfr", NULL);
    debug_printf(DEBUG_WARNING,"Found url: %s\n", url);
	gtk_icon_theme_append_search_path(gtk_icon_theme_get_default (),url);
    g_free(url);
    g_free(path);

    /* Add plugin path */
//	gtk_icon_theme_append_search_path(gtk_icon_theme_get_default (),DATA_DIR);

    g_assert(lfm == NULL);
    lfm = last_fm_new();
}
static void lfr_destroy(void)
{
    /* remove the extra reference */
    if(lfr_vbox) {g_object_unref(lfr_vbox);lfr_vbox = NULL;}
    if(lfm) {g_object_unref(lfm);lfm = NULL;}
}

static void lfr_save_myself(void)
{
    /* Save the position in the left tree */
	if (lfr_ref)
    {
        GtkTreePath *path = gtk_tree_row_reference_get_path(lfr_ref);
        if(path)
        {
            gint *indices = gtk_tree_path_get_indices(path);
            debug_printf(DEBUG_INFO,"Saving myself '%s' to position: %i\n",plugin.name, indices[0]);
            cfg_set_single_value_as_int(config, CONFIG_NAME,"position",indices[0]);
            gtk_tree_path_free(path);
        }
    }
}

/**
 * The actual browser
 */
static GtkWidget *connection_warning_box = NULL;
static GtkWidget *rest_box = NULL;
static GtkWidget *user_label = NULL;
static GtkWidget *my_station_combo = NULL;
static GtkWidget *progressbar = NULL;
static GtkWidget *song_info_box = NULL;

static void lfr_browser_conn_changed(LastFM *l, gboolean connected,gpointer data)
{
    if(connected)  {
        gchar *str = g_markup_printf_escaped("<span size='xx-large'>%s's Last.FM Dashboard</span>", 
                last_fm_get_username(lfm));

        gtk_label_set_markup(GTK_LABEL(user_label),str);
        g_free(str);
        gtk_widget_show_all(rest_box);
        gtk_widget_hide(connection_warning_box);
    }else{
        gtk_widget_hide(rest_box);
        gtk_widget_show_all(connection_warning_box);
    }
}
void play_path(const gchar *path);
static void lfr_browser_play_stream(LastFM *l, const gchar *stream, gpointer data)
{
    play_path(stream);
}
static void lfr_browser_error_callback(LastFM *l,gboolean fatal, const gchar *errormsg, gpointer data)
{
    playlist3_show_error_message(errormsg, (fatal)?ERROR_CRITICAL:ERROR_WARNING);
}
guint pb_timeout = 0;
static gboolean lfr_browser_pulse(GtkProgressBar *pb)
{
    gtk_progress_bar_pulse(GTK_PROGRESS_BAR(pb));
    return TRUE;
}
static void lfr_browser_processing(LastFM *l, gboolean processing, gpointer data)
{
    if(processing)
    {
        gtk_widget_set_sensitive(rest_box, FALSE);
        gtk_widget_show(progressbar);
        if(pb_timeout == 0) {
            pb_timeout = g_timeout_add(100, (GSourceFunc)lfr_browser_pulse, progressbar);
        }
    }else{
        gtk_widget_set_sensitive(rest_box, TRUE);
        gtk_widget_hide(progressbar);
        if(pb_timeout)
            g_source_remove(pb_timeout);
        pb_timeout = 0;
    }
}
extern GmpcConnection 	*gmpcconn;

static void lfr_browser_song_info(LastFM *l, mpd_Song *song, gpointer data)
{
    GtkWidget *label = NULL;
    GtkWidget *hbox = gtk_table_new(3, 3,FALSE);
    GtkWidget *temp = gtk_bin_get_child(GTK_BIN(song_info_box));

    if(cfg_get_single_value_as_int_with_default(config, CONFIG_NAME, "dirty-hack", FALSE) == 0)
    {
        if(temp)
            gtk_widget_destroy(temp); 
        if(song)
        {
            gtk_table_set_row_spacings(GTK_TABLE(hbox), 6);
            gtk_table_set_col_spacings(GTK_TABLE(hbox), 6);
            gtk_container_set_border_width(GTK_CONTAINER(hbox), 6);
            /* artist */
            label = gtk_label_new("");
            gtk_misc_set_alignment(GTK_MISC(label), 1.0,0.5);
            gtk_label_set_markup(GTK_LABEL(label),"<b>Track</b>");
            gtk_table_attach(GTK_TABLE(hbox), label,1,2,0,1,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);
            label = gtk_label_new(song->title);
            gtk_misc_set_alignment(GTK_MISC(label), 0.0,0.5);
            gtk_table_attach(GTK_TABLE(hbox), label,2,3,0,1,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);

            label = gtk_label_new("");
            gtk_misc_set_alignment(GTK_MISC(label), 1.0,0.5);
            gtk_label_set_markup(GTK_LABEL(label),"<b>Artist</b>");
            gtk_table_attach(GTK_TABLE(hbox), label,1,2,1,2,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);
            label = gtk_label_new(song->artist);
            gtk_misc_set_alignment(GTK_MISC(label), 0.0,0.5);
            gtk_table_attach(GTK_TABLE(hbox), label,2,3,1,2,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);

            label = gtk_label_new("");
            gtk_misc_set_alignment(GTK_MISC(label), 1.0,0.5);
            gtk_label_set_markup(GTK_LABEL(label),"<b>Album</b>");
            gtk_table_attach(GTK_TABLE(hbox), label,1,2,2,3,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);
            label = gtk_label_new(song->album);
            gtk_misc_set_alignment(GTK_MISC(label), 0.0,0.5);
            gtk_table_attach(GTK_TABLE(hbox), label,2,3,2,3,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);

            label = gmpc_metaimage_new_size(META_ALBUM_ART,96);
            gmpc_metaimage_update_cover_from_song(GMPC_METAIMAGE(label), song);
            gtk_table_attach(GTK_TABLE(hbox), label,0,1,0,3,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);

            gtk_widget_show_all(hbox);
            gtk_container_add(GTK_CONTAINER(song_info_box), hbox);
        }
    }
    else if(song && song->artist)
    {   
        mpd_Song *song2 = mpd_playlist_get_current_song(connection);
        int changed = 0;
        if(song2->artist){
            if(song->artist) changed += (strcmp(song2->artist, song->artist) != 0);
            else changed+=1; 
            g_free(song2->artist);
            song2->artist = g_strdup(song->artist);
        }else if(song->artist) {
            changed+=1;
            song2->artist = g_strdup(song->artist);
        }
        if(song2->album){
            if(song->album) changed += (strcmp(song2->album, song->album) != 0);
            else changed+=1; 
            g_free(song2->album);
            song2->album = g_strdup(song->album);
        }else if (song->album)
        {
            changed+=1;
            song2->album = g_strdup(song->album);
        }
        if(song2->title){
            if(song->title) changed += (strcmp(song2->title, song->title) != 0);
            else changed+=1; 
            g_free(song2->title);
            song2->title = g_strdup(song->title);
        }else if (song->title)
        {
            changed+=1;
            song2->title = g_strdup(song->title);
        }
        if(changed)
        {
            gmpc_connection_status_changed(gmpcconn,connection, MPD_CST_SONGID|MPD_CST_SONGPOS); 
            if(temp)
                gtk_widget_destroy(temp); 
            if(song)
            {
                gtk_table_set_row_spacings(GTK_TABLE(hbox), 6);
                gtk_table_set_col_spacings(GTK_TABLE(hbox), 6);
                gtk_container_set_border_width(GTK_CONTAINER(hbox), 6);
                /* artist */
                label = gtk_label_new("");
                gtk_misc_set_alignment(GTK_MISC(label), 1.0,0.5);
                gtk_label_set_markup(GTK_LABEL(label),"<b>Track</b>");
                gtk_table_attach(GTK_TABLE(hbox), label,1,2,0,1,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);
                label = gtk_label_new(song->title);
                gtk_misc_set_alignment(GTK_MISC(label), 0.0,0.5);
                gtk_table_attach(GTK_TABLE(hbox), label,2,3,0,1,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);

                label = gtk_label_new("");
                gtk_misc_set_alignment(GTK_MISC(label), 1.0,0.5);
                gtk_label_set_markup(GTK_LABEL(label),"<b>Artist</b>");
                gtk_table_attach(GTK_TABLE(hbox), label,1,2,1,2,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);
                label = gtk_label_new(song->artist);
                gtk_misc_set_alignment(GTK_MISC(label), 0.0,0.5);
                gtk_table_attach(GTK_TABLE(hbox), label,2,3,1,2,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);

                label = gtk_label_new("");
                gtk_misc_set_alignment(GTK_MISC(label), 1.0,0.5);
                gtk_label_set_markup(GTK_LABEL(label),"<b>Album</b>");
                gtk_table_attach(GTK_TABLE(hbox), label,1,2,2,3,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);
                label = gtk_label_new(song->album);
                gtk_misc_set_alignment(GTK_MISC(label), 0.0,0.5);
                gtk_table_attach(GTK_TABLE(hbox), label,2,3,2,3,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);

                label = gmpc_metaimage_new_size(META_ALBUM_ART,96);
                gmpc_metaimage_update_cover_from_song(GMPC_METAIMAGE(label), song);
                gtk_table_attach(GTK_TABLE(hbox), label,0,1,0,3,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);

                gtk_widget_show_all(hbox);
                gtk_container_add(GTK_CONTAINER(song_info_box), hbox);
            }
        }
    }
    else
    {
        if(temp)
            gtk_widget_destroy(temp); 


    }
}
gulong lfm_update_timeout  = 0;
static int lfm_update(LastFM *l)
{
    last_fm_get_current_song(lfm);
    return TRUE;
}
static void lfr_browser_song_info_query(GtkWidget *button, gpointer data)
{
    mpd_Song *song = mpd_playlist_get_current_song(connection);
    if(song && song->file)
    {
        const gchar *stream_url = last_fm_get_stream_url(lfm);
        if(stream_url && strcmp(stream_url, song->file) == 0)
        {
            last_fm_get_current_song(lfm);
            if(lfm_update_timeout == 0) {
                if(cfg_get_single_value_as_int_with_default(config, CONFIG_NAME, "dirty-hack", FALSE))
                    lfm_update_timeout = g_timeout_add_seconds(15, G_CALLBACK(lfm_update), lfm); 
            }
            return;
        }
            
    }
    lfr_browser_song_info(lfm, NULL, NULL);
    if(lfm_update_timeout)
    {
        g_source_remove(lfm_update_timeout);
        lfm_update_timeout = 0;
    }
}
static void   lfr_mpd_status_changed(MpdObj *mi, ChangedStatusType what, void *data)
{
    if(what&MPD_CST_SONGPOS || what&MPD_CST_SONGPOS)
    {
        if(song_info_box)
            lfr_browser_song_info_query(NULL, NULL);
    }
}
static void lfr_button_clicked(GtkWidget *button, GtkWidget *entry)
{
    const gchar *combotext = gtk_combo_box_get_active_text(GTK_COMBO_BOX(my_station_combo));
    const gchar *text = gtk_entry_get_text(GTK_ENTRY(entry));
    if(strlen(text) > 0 && combotext)
    {
        if(strcmp(combotext, "Artist") == 0) {
            last_fm_play_artist_radio(lfm, text);
        } else if(strcmp(combotext, "Similar Artist") == 0) {
            last_fm_play_similar_artist_radio(lfm, text);
        } else if(strcmp(combotext, "Tag") == 0) {
            last_fm_play_tag_radio(lfm, text);
        } else if(strcmp(combotext, "User") == 0) {
            last_fm_play_user_radio(lfm, text);
        } else if(strcmp(combotext, "Group") == 0) {
            last_fm_play_group_radio(lfm, text);
        } else if(strcmp(combotext, "Neighbourhood") == 0) {
            last_fm_play_neighbours_radio(lfm, text);
        } 
    }
}

static void lfr_button_play_recommendations(GtkWidget *button, gpointer data) {
    last_fm_play_recommendations(lfm);
}
static void lfr_button_play_my_radio_station(GtkWidget *button, gpointer data) {
    last_fm_play_user_radio(lfm,NULL);
}
static void  lfr_button_play_my_neighbourhood(GtkWidget *button, gpointer data) {
    last_fm_play_neighbours_radio(lfm, NULL);
}
static void  lfr_button_play_my_loved(GtkWidget *button, gpointer data) {
    last_fm_play_loved_radio (lfm);
}

void preferences_show_pref_window(int plugin_id);
static void lfr_open_preferences(GtkWidget *but, gpointer data)
{
    preferences_show_pref_window(plugin.id);
}
/*
 * Styles
 */
static void lfr_background_style_changed(GtkWidget *vbox, GtkStyle *style,  GtkWidget *vp)
{
	gtk_widget_modify_bg(vp,GTK_STATE_NORMAL, &(GTK_WIDGET(vbox)->style->base[GTK_STATE_NORMAL]));
}
static gboolean lfr_heading_expose_event(GtkWidget *widget, GdkEventExpose *event, gpointer data)
{

	int width = widget->allocation.width;
	int height = widget->allocation.height;
	
	gtk_paint_flat_box(widget->style, 
					widget->window, 
					GTK_STATE_SELECTED,
					GTK_SHADOW_NONE,
					NULL, 
					widget,
					"cell_odd",
					0,0,
					width,height);

	gtk_paint_focus(widget->style, widget->window, 
				GTK_STATE_NORMAL, 
				NULL, 
				widget,
				"button",
				0,0,width,height);
	return FALSE;
}


static void lfr_browser_init() 
{
    GtkWidget *hbox;
    GtkWidget *temp, *temp2;
    GtkWidget *event = NULL;
    /* If the widget allready exists, don't try to create it. */
    if(lfr_vbox != NULL) return;
    

        /**
         * VBOX <holds everything>
         *  + 
         *    - Header
         *    + Frame
         *      + Event box (for bg colour)
         *        - <content>
         *  + 
         *    - Warning box 
         */
    lfr_vbox = gtk_vbox_new(FALSE, 0);
    /* Header */
    event = gtk_event_box_new();
    gtk_widget_set_app_paintable(event, TRUE);
    g_signal_connect(G_OBJECT(event), "expose-event", G_CALLBACK(lfr_heading_expose_event), NULL);

    hbox = gtk_hbox_new(FALSE, 6);
    gtk_container_set_border_width(GTK_CONTAINER(hbox), 6);

    temp = gtk_image_new_from_icon_name("lastfm", GTK_ICON_SIZE_DND);
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE, TRUE, 0);   

    temp = gtk_label_new(""); 
    gtk_label_set_markup(GTK_LABEL(temp), "<span size='xx-large' weight='bold'>Last.FM Radio</span>");
    gtk_misc_set_alignment(GTK_MISC(temp), 0.0, 0.5);
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(event), hbox);
    gtk_box_pack_start(GTK_BOX(lfr_vbox), event, FALSE, TRUE, 0);
    gtk_widget_show_all(event);


    /* Event box for colouring back */
    temp = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(temp), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
    event = gtk_event_box_new();
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(temp), GTK_SHADOW_IN);
    gtk_widget_modify_bg(event,GTK_STATE_NORMAL, &(GTK_WIDGET(lfr_vbox)->style->base[GTK_STATE_NORMAL]));
    g_signal_connect(G_OBJECT(lfr_vbox), "style-set", G_CALLBACK(lfr_background_style_changed), event);


    rest_box = gtk_vbox_new(FALSE, 6);
    gtk_container_set_border_width(GTK_CONTAINER(rest_box), 8);
    gtk_container_add(GTK_CONTAINER(event), rest_box);
    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(temp), event);
    gtk_box_pack_start(GTK_BOX(lfr_vbox), temp, TRUE, TRUE, 0);
    
    gtk_widget_show(event);
    gtk_widget_show(temp);



    /** 
     * Content
     */
    user_label = gtk_label_new("");
    gtk_misc_set_alignment(GTK_MISC(user_label), 0.0,0.5);
    gtk_box_pack_start(GTK_BOX(rest_box), user_label, FALSE,TRUE,12);


    temp = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(temp),"<span size='x-large'>My Stations</span>");
    gtk_misc_set_alignment(GTK_MISC(temp), 0.0,0.5);
    gtk_box_pack_start(GTK_BOX(rest_box), temp, FALSE,TRUE,0);


    /* My recommendations */
    hbox = gtk_hbox_new(FALSE, 0);
    temp = gtk_image_new_from_icon_name("recommended_radio", GTK_ICON_SIZE_MENU);
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE,TRUE,0); 
    temp = gtk_label_new("My Recommendations");
    temp2 = gtk_button_new();
    gtk_container_add(GTK_CONTAINER(temp2), temp);
    gtk_button_set_relief(GTK_BUTTON(temp2), GTK_RELIEF_NONE);
    gtk_box_pack_start(GTK_BOX(hbox), temp2, FALSE,TRUE,0); 
    gtk_box_pack_start(GTK_BOX(rest_box), hbox, FALSE,TRUE,0);
    g_signal_connect(G_OBJECT(temp2), "clicked", G_CALLBACK(lfr_button_play_recommendations), NULL);

    /* My Radio Station*/
    hbox = gtk_hbox_new(FALSE, 0);
    temp = gtk_image_new_from_icon_name("personal_radio", GTK_ICON_SIZE_MENU);
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE,TRUE,0); 
    temp = gtk_label_new("My Radio Station");
    temp2 = gtk_button_new();
    gtk_container_add(GTK_CONTAINER(temp2), temp);
    gtk_button_set_relief(GTK_BUTTON(temp2), GTK_RELIEF_NONE);
    gtk_box_pack_start(GTK_BOX(hbox), temp2, FALSE,TRUE,0); 
    gtk_box_pack_start(GTK_BOX(rest_box), hbox, FALSE,TRUE,0);
    g_signal_connect(G_OBJECT(temp2), "clicked", G_CALLBACK(lfr_button_play_my_radio_station), NULL);


    /* My Radio Station*/
    hbox = gtk_hbox_new(FALSE, 0);
    temp = gtk_image_new_from_icon_name("my_neighbours", GTK_ICON_SIZE_MENU);
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE,TRUE,0); 
    temp = gtk_label_new("My Neighbourhood");
    temp2 = gtk_button_new();
    gtk_container_add(GTK_CONTAINER(temp2), temp);
    gtk_button_set_relief(GTK_BUTTON(temp2), GTK_RELIEF_NONE);
    gtk_box_pack_start(GTK_BOX(hbox), temp2, FALSE,TRUE,0); 
    gtk_box_pack_start(GTK_BOX(rest_box), hbox, FALSE,TRUE,0);
    g_signal_connect(G_OBJECT(temp2), "clicked", G_CALLBACK(lfr_button_play_my_neighbourhood), NULL);

    /* My Radio Station*/
    hbox = gtk_hbox_new(FALSE, 0);
    temp = gtk_image_new_from_icon_name("loved_radio", GTK_ICON_SIZE_MENU);
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE,TRUE,0); 
    temp = gtk_label_new("My Loved Songs");
    temp2 = gtk_button_new();
    gtk_container_add(GTK_CONTAINER(temp2), temp);
    gtk_button_set_relief(GTK_BUTTON(temp2), GTK_RELIEF_NONE);
    gtk_box_pack_start(GTK_BOX(hbox), temp2, FALSE,TRUE,0); 
    gtk_box_pack_start(GTK_BOX(rest_box), hbox, FALSE,TRUE,0);
    g_signal_connect(G_OBJECT(temp2), "clicked", G_CALLBACK(lfr_button_play_my_loved), NULL);


    /* */
    temp = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(temp),"<span size='x-large'>Create Radio Station</span>");
    gtk_misc_set_alignment(GTK_MISC(temp), 0.0,0.5);
    gtk_box_pack_start(GTK_BOX(rest_box), temp, FALSE,TRUE,0);



    progressbar = gtk_progress_bar_new();
    gtk_box_pack_start(GTK_BOX(lfr_vbox), progressbar, FALSE,TRUE,0);

    /* Quick and dirty box */
    hbox = gtk_hbox_new(FALSE, 6);

    my_station_combo = temp = gtk_combo_box_new_text();
    gtk_combo_box_append_text(GTK_COMBO_BOX(temp), "Artist");
    gtk_combo_box_append_text(GTK_COMBO_BOX(temp), "Similar Artist");
    gtk_combo_box_append_text(GTK_COMBO_BOX(temp), "Tag");
    gtk_combo_box_append_text(GTK_COMBO_BOX(temp), "User");
    gtk_combo_box_append_text(GTK_COMBO_BOX(temp), "Group");
    gtk_combo_box_append_text(GTK_COMBO_BOX(temp), "Neighbourhood");
    gtk_combo_box_set_active(GTK_COMBO_BOX(temp), 1);
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE,TRUE,0);

    temp = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(hbox), temp, TRUE,TRUE,0);
    g_signal_connect(G_OBJECT(temp), "activate", G_CALLBACK(lfr_button_clicked), temp);
    temp2 = gtk_button_new_from_stock(GTK_STOCK_MEDIA_PLAY);
    gtk_box_pack_start(GTK_BOX(hbox), temp2, FALSE,TRUE,0);
    g_signal_connect(G_OBJECT(temp2), "clicked", G_CALLBACK(lfr_button_clicked), temp);

    gtk_box_pack_start(GTK_BOX(rest_box), hbox, FALSE,TRUE,0);


    song_info_box = gtk_frame_new(NULL); 
    gtk_box_pack_start(GTK_BOX(rest_box), song_info_box, FALSE,TRUE,0);

    hbox = gtk_hbox_new(FALSE, 0);
    temp = gtk_image_new_from_icon_name(GTK_STOCK_REFRESH, GTK_ICON_SIZE_MENU);    
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE,TRUE,0); 
    temp2 = gtk_button_new_with_label("Query current song info");
    gtk_button_set_relief(GTK_BUTTON(temp2), GTK_RELIEF_NONE);
    gtk_box_pack_start(GTK_BOX(hbox), temp2, FALSE,TRUE,0); 
    gtk_box_pack_start(GTK_BOX(rest_box), hbox, FALSE,TRUE,0);
    g_signal_connect(temp2, "clicked", G_CALLBACK(lfr_browser_song_info_query), NULL);

    /** Show this box when we are not connected */
    hbox = gtk_hbox_new(FALSE, 6);
    connection_warning_box = gtk_frame_new(NULL);
    gtk_container_add(GTK_CONTAINER(connection_warning_box), hbox);
    gtk_container_set_border_width(GTK_CONTAINER(hbox), 8);
    /* Pack it */
    gtk_box_pack_end(GTK_BOX(lfr_vbox), connection_warning_box, FALSE, TRUE, 0);
    temp = gtk_image_new_from_stock(GTK_STOCK_DIALOG_WARNING, GTK_ICON_SIZE_DIALOG);
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE, TRUE, 0);
    temp = gtk_label_new("You must enter a valid last.FM username and pasword");
    gtk_misc_set_alignment(GTK_MISC(temp), 0.0,0.5);
    gtk_box_pack_start(GTK_BOX(hbox), temp, TRUE, TRUE, 0);
    temp = gtk_button_new_with_label("Open preferences");
    gtk_box_pack_start(GTK_BOX(hbox), temp, FALSE, TRUE, 0);   
    g_signal_connect(G_OBJECT(temp), "clicked", G_CALLBACK(lfr_open_preferences), NULL);   

    if(last_fm_is_connected(lfm))  {
        gtk_widget_show_all(rest_box);
    }else{
        gtk_widget_show_all(connection_warning_box);
    }

    gtk_widget_show(lfr_vbox);
    /* Add extra references so the widget is not destroyed when removed*/
    g_object_ref(lfr_vbox);


    g_signal_connect(G_OBJECT(lfm), "connection_changed", G_CALLBACK(lfr_browser_conn_changed), NULL);
    g_signal_connect(G_OBJECT(lfm), "play_stream", G_CALLBACK(lfr_browser_play_stream), NULL);
    g_signal_connect(G_OBJECT(lfm), "processing", G_CALLBACK(lfr_browser_processing), NULL);

    g_signal_connect(G_OBJECT(lfm), "song_info_available", G_CALLBACK(lfr_browser_song_info), NULL);
    g_signal_connect(G_OBJECT(lfm), "error_callback", G_CALLBACK(lfr_browser_error_callback), NULL);

    last_fm_connect(lfm);
}

/**
 * Browser integration functions
 */
/* When the player needs to be added to the right pane  */
static void lfr_browser_selected(GtkWidget *container)
{
	if(lfr_vbox == NULL)
	{
		lfr_browser_init();
    }
    gtk_container_add(GTK_CONTAINER(container), lfr_vbox);
    gtk_widget_show(lfr_vbox);
}
/* When the player needs to be removed from the right pane  */
static void lfr_browser_unselected(GtkWidget *container)
{
	gtk_container_remove(GTK_CONTAINER(container), gtk_bin_get_child(GTK_BIN(container)));
}
/* Add the plugin to the left pane */
static void lfr_browser_add(GtkWidget *cat_tree)
{
	GtkTreePath *path = NULL;
	GtkTreeIter iter;
	GtkListStore *pl3_tree = (GtkListStore *)gtk_tree_view_get_model(GTK_TREE_VIEW(cat_tree));	
	gint pos = cfg_get_single_value_as_int_with_default(config, CONFIG_NAME,"position",20);

	if(!cfg_get_single_value_as_int_with_default(config, CONFIG_NAME, "enable", TRUE)) return;

    debug_printf(DEBUG_INFO,"Adding at position: %i", pos);
	playlist3_insert_browser(&iter, pos);
	gtk_list_store_set(GTK_LIST_STORE(pl3_tree), &iter, 
			PL3_CAT_TYPE, plugin.id,
			PL3_CAT_TITLE, "Last.FM Radio",
			PL3_CAT_INT_ID, "",
			PL3_CAT_ICON_ID, "lastfm",
			-1);
	/**
	 * Clean up old row reference if it exists
	 */
	if (lfr_ref)
	{
		gtk_tree_row_reference_free(lfr_ref);
		lfr_ref = NULL;
	}
	/**
	 * create row reference
	 */
	path = gtk_tree_model_get_path(GTK_TREE_MODEL(playlist3_get_category_tree_store()), &iter);
	if (path)
	{
		lfr_ref = gtk_tree_row_reference_new(GTK_TREE_MODEL(playlist3_get_category_tree_store()), path);
		gtk_tree_path_free(path);
	}
}
/**
 * Enable Set & Get
 */
static int lfr_get_enabled()
{
	return cfg_get_single_value_as_int_with_default(config, CONFIG_NAME, "enable", TRUE);
}
void pl3_update_go_menu(void);
static void lfr_set_enabled(int enabled)
{
	cfg_set_single_value_as_int(config, CONFIG_NAME, "enable", enabled);
	if (enabled)
	{
        /* Add the browser to the left pane, if there is none to begin with */
		if(lfr_ref == NULL)
		{
			lfr_browser_add(GTK_WIDGET(playlist3_get_category_tree_view()));
		}
	}
	else if (lfr_ref)
	{
        /* Remove it from the left pane */
		GtkTreePath *path = gtk_tree_row_reference_get_path(lfr_ref);
		if (path){
			GtkTreeIter iter;
            /* for a save of myself */
            lfr_save_myself();
			if (gtk_tree_model_get_iter(GTK_TREE_MODEL(playlist3_get_category_tree_store()), &iter, path)){
				gtk_list_store_remove(playlist3_get_category_tree_store(), &iter);
			}
			gtk_tree_path_free(path);
			gtk_tree_row_reference_free(lfr_ref);
			lfr_ref = NULL;
		}                                                                                                  	
	}                                                                                                      	
    /* Make sure the go menu is in sync */
	pl3_update_go_menu();
}
/**
 * Preferences
 */
gulong id = 0;
GtkWidget *username_entry;
GtkWidget *password_entry;
static void lfr_pref_set_info(GtkWidget *button, gpointer data)
{
    const gchar *username = gtk_entry_get_text(GTK_ENTRY(username_entry));
    const gchar *password = gtk_entry_get_text(GTK_ENTRY(password_entry));
    if(strlen(username) > 0 && strlen(password) > 0)
    {
        last_fm_set_login(lfm, username, password);        
        last_fm_connect(lfm);
    }
}
static void lfr_pref_conn_changed(LastFM *l, gboolean connected, GtkWidget *label)
{
    gtk_label_set_text(GTK_LABEL(label),connected?"connected":"disconnected");
}
static void lfr_pref_construct(GtkWidget *container)
{
    GtkWidget *table = gtk_table_new(3,2,FALSE);
    GtkWidget *label;

    gtk_table_set_row_spacings(GTK_TABLE(table),6);
    gtk_table_set_col_spacings(GTK_TABLE(table),6);

    label = gtk_label_new("Username");
    gtk_misc_set_alignment(GTK_MISC(label), 1.0,0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0,1,0,1,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);
    username_entry = gtk_entry_new();
    if(last_fm_get_username(lfm) != NULL) gtk_entry_set_text(GTK_ENTRY(username_entry),last_fm_get_username(lfm));
    gtk_table_attach(GTK_TABLE(table), username_entry, 1,2,0,1,GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_SHRINK, 0,0);


    label = gtk_label_new("Password");
    gtk_misc_set_alignment(GTK_MISC(label), 1.0,0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0,1,1,2,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);
    password_entry = gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(password_entry), FALSE);
    gtk_table_attach(GTK_TABLE(table), password_entry, 1,2,1,2,GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_SHRINK, 0,0);


    label = gtk_button_new_with_label("Set information");
    gtk_table_attach(GTK_TABLE(table), label, 1,2,2,3,GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_SHRINK, 0,0);
    g_signal_connect(G_OBJECT(label), "clicked", G_CALLBACK(lfr_pref_set_info), NULL);

    label = gtk_label_new(last_fm_is_connected(lfm)?"connected":"disconnected");
    gtk_table_attach(GTK_TABLE(table), label, 0,1,2,3,GTK_FILL, GTK_FILL|GTK_SHRINK, 0,0);

    gtk_container_add(GTK_CONTAINER(container), GTK_WIDGET(GTK_TABLE(table)));
    gtk_widget_show_all(table);

    id = g_signal_connect(lfm, "connection_changed",G_CALLBACK(lfr_pref_conn_changed), label);
}
static void lfr_pref_destroy(GtkWidget *container)
{
    g_signal_handler_disconnect(lfm, id);
    id = 0;
	gtk_container_remove(GTK_CONTAINER(container), gtk_bin_get_child(GTK_BIN(container)));
}

gmpcPrefPlugin lfr_pref = {
    .construct = lfr_pref_construct,
    .destroy = lfr_pref_destroy
};
/**
 * Structure used by gmpc for integrating the browser
 */
gmpcPlBrowserPlugin lfr_gbp = {
	.add                    = lfr_browser_add,
	.selected               = lfr_browser_selected,
	.unselected             = lfr_browser_unselected,
};

/**
 * This allows gmpc to check the api this plugin is compiled against 
 */
int plugin_api_version = PLUGIN_API_VERSION;
/**
 * The plugin structure 
 */
gmpcPlugin plugin = {
    .name                   = "Last.FM Radio",
    .version                = {PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION},
    .plugin_type            = GMPC_PLUGIN_PL_BROWSER,
    /* Creation and destruction */
    .init                   = lfr_init,
    .destroy                = lfr_destroy,
    .save_yourself          = lfr_save_myself,
    .mpd_status_changed     = lfr_mpd_status_changed, 
    /* Browser extention */
    .browser                = &lfr_gbp,
    /* Preferences pane */
    .pref                   = &lfr_pref,

    /* Get/set enabled */
    .get_enabled            = lfr_get_enabled,
    .set_enabled            = lfr_set_enabled
};
